// Cycle15.4.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

int main()
{
    
    int Limit;
    bool IsOdd;
    char UserAnswer;
    
    std::cout << "Hello! I can show even or odd numbers. \n" << 
        "Insert number" << "\n";
    std::cin >> Limit;
    std::cout << "If you can odd number insert [y]," <<
        "if you can even number insert [n]" << "\n";
    std::cin >> UserAnswer;
    IsOdd = UserAnswer == 'y';
    std::cout << "Here is numbers for " << Limit << ':' << "\n";

    int FindOddNumbers(int Limit, bool IsOdd);
    {       
        for (int number = 0; number + IsOdd <= Limit; number += 2 )
            std::cout << number + IsOdd << "\n";

        return 0;
    }
}

